var colonyArray = [];
var userArray = [];

// Returns a new Google Maps Map object
function createMap(mapElement){
    return new google.maps.Map(mapElement, {
        zoom: 10,
        center: { lat: 52.3555, lng: 1.1743 }
    });
}

// Zooming the map to the user's position
function zoomToUser(map){
    infoWindow = new google.maps.InfoWindow;

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };

        infoWindow.setPosition(pos);
        infoWindow.setContent('You are here.');
        infoWindow.open(map);
        map.setCenter(pos);
        }, function() {
        handleLocationError(true, infoWindow, map);
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map);
    }
}

// An error handler for location errors while zooming to user
function handleLocationError(browserHasGeolocation, infoWindow, map) {
    infowindow = new google.maps.InfoWindow;
    infoWindow.setPosition(map.getCenter());
    infoWindow.setContent(browserHasGeolocation ?
                          'The Geolocation service failed. You probably didn\'t give us permession.' :
                          'Your browser doesn\'t support geolocation.');
    map.setOptions({zoom: 2});
    infoWindow.open(map);
}

// Create the marker clusterer
function createClusterer(map, markers, mapElement){
    var markerCluster = new MarkerClusterer(map, markers, { minimumClusterSize: 50 });

    // Mouse over listener
    google.maps.event.addListener(markerCluster, "mouseover", function (mapElement) {
        // Info Window
        mapElement.infowindow = new google.maps.InfoWindow({
            content: "Unknown"
        });

        var mm = mapElement.getSize();
        var text1 = mapElement.firetotals.toString(); var text2 = mapElement.treetotals.toString(); var text3 = mapElement.swamptotals.toString();
        mapElement.infowindow.setContent("<p style = 'color:red'>Fire " + text1 + "</p>" + "<p style = 'color:green'> Leaf " + text2 + "</p>" +
            "<p style = 'color:blue'>Swamp " + text3 + "</p>");
        mapElement.infowindow.setPosition(mapElement.getCenter());
        mapElement.infowindow.open(map);
    });

    // Mouse out listener
    google.maps.event.addListener(markerCluster, "mouseout", function (mapElement) {
        mapElement.infowindow.close();
    });
}

// Return call of the Google Maps API
function initMap() {

    // Create Variables
    var mapElement = document.getElementById("map");
    var map = createMap(mapElement);
    var markers = [];

    User.LoadUsers();
    User.SetCircleUser();
    zoomToUser(map);

    // Loop through the colonies
    for (i in colonies) {
        var colony = new Colony(colonies[i]);
        window.colonyArray.push(colony);
        marker = colony.getMarker(map);
        markers.push(marker);
    }

    // Cluster the markers
    var markerCluster = createClusterer(map, markers, mapElement);
}