// The user class contains all the code and data relevant to the users
class User{
    // The constructor takes the user object from the json file and converts it to something more userfriendly
    constructor(oldUser){
        this.ID = oldUser.i;
        this.username = oldUser.n;
        this.score = oldUser.w;
    }

    // Returns the colonies associated with the user
    get Colonies(){
        var UserColonies = [];
        for(var i in window.colonyArray){
            if(window.colonyArray[i].owner == this.ID){
                UserColonies.push(window.colonyArray[i]);
            }
        }
        return UserColonies;
    }

    // Returns the number of ants a user has in their colonies
    get Ants(){
        // If this value has previously been used, we send back the precalculated value
        if(this.ants != null){
            return this.ants;
        }

        var colonies = this.Colonies;
        var counter = 0;
        for(var i in colonies){
            counter += colonies[i].ants;
        }
        
        // Set a variable in the object which we use if asked for again
        this.ants = counter;

        return counter;
    }

    // Get the rank of the user
    get Rank(){
        return window.userArray.indexOf(this);
    }

    // Loads all the users into a global array
    static LoadUsers(){
        window.userArray = [];
        for(var i in users){
            var user = new User(users[i]);
            window.userArray.push(user);
        }
    }

    // Returns a user object by a given username
    static GetByUsername(username){
        for(var i in window.userArray){
            if(window.userArray[i].username == username){
                return window.userArray[i];
            }
        }
    }

    // Returns a user object by a given ID
    static GetByID(ID){
        if(window.userArray == null){
            this.LoadUsers();
        }

        for(var i in window.userArray){
            if(window.userArray[i].ID == ID){
                return window.userArray[i];
            }
        }
    }

    // Sets for which user the circles have to be drawn
    static SetCircleUser(){
        var user = User.GetByID(new URL(window.location.href).searchParams.get("user"));
        if(user == null || user.ID == 0){
            user = User.GetByUsername(new URL(window.location.href).searchParams.get("username"));
        }
        console.log(user);
        window.user = user;
    }
}