# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Development of a web-based global map for the DomonAnt Android application.  
* J. Loftus w Erik Melkersson  
* Version 1.0 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You just need the files all in the same directory. 
* Uses google Maps, the  google.maps.marker API, and a modified version of the markerclusterplus Javascript library from
* https://github.com/googlemaps/v3-utility-library/tree/master/markerclustererplus
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact