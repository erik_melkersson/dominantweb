// The colony class countains the info of a colony
class Colony {
    // Takes the oldColony object provided by the json and converts it to something useful
    constructor(oldcolony) {
        this.species = oldcolony.s;
        this.owner = oldcolony.o;
        this.ants = oldcolony.w;
        this.name = oldcolony.n;

        this.countryCode = oldcolony.a0;
        this.stateCode = oldcolony.a1;
        this.municipalityCode = oldcolony.a2;

        this.position = { lat: oldcolony.lat, lng: oldcolony.lng };
    }

    // Get the image to be used on the map
    get image() {
        switch (this.species) {
            case 1: return { url: 'colony1.png', scaledSize: new google.maps.Size(30, 30) }; break;
            case 2: return { url: 'colony2.png', scaledSize: new google.maps.Size(30, 30) }; break;
            case 3: return { url: 'colony3.png', scaledSize: new google.maps.Size(30, 30) }; break;
            case 4: return { url: 'colony4.png', scaledSize: new google.maps.Size(30, 30) }; break;
        }
    }

    // Get the country the colony is in
    get country(){
        for(var i in areas0){
            if(areas0[i].i == this.countryCode){
                return areas0[i].n;
            }
        }
        return "Unknown";
    }

    // Get the state the colony is in
    get state(){
        for(var i in areas1){
            if(areas1[i].i == this.stateCode){
                return areas1[i].n;
            }
        }
        return "Unknown";
    }

    // Get the municipality the colony is in
    get municipality(){
        for(var i in areas2){
            if(areas2[i].i == this.municipalityCode){
                return areas2[i].n;
            }
        }
        return "Unknown";
    }

    // Create the marker for the colony and return it
    getMarker(map) {
        if (this.marker != null) {
            return this.marker;
        }

        var marker = new google.maps.Marker({
            position: this.position,
            icon: this.image,
            map: map,
            team: this.species,
            owner: this.owner,
            ants: this.ants
        });

        if (marker.owner == document.getElementById("userpicker").elements["user"].value) {
            marker.circle = this.getMarkerCircle(map);
        }

        marker.infowindow = new google.maps.InfoWindow({
            closeBoxURL: "",
            closeBoxMargin: "",
            content: "Unknown Player"
        });

        var that = this;

        google.maps.event.addListener(marker, 'mouseover', function () {
            that.createMouseOverWindow(this, map);
        });
        google.maps.event.addListener(marker, 'mouseout', function () {
            this.infowindow.close();
        });

        this.marker = marker;

        return marker;
    }

    // Create the marker circle around the colony
    getMarkerCircle(map) {
        var nestCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: this.position,
            radius: 200
        });

        return nestCircle;
    }

    // Create the mouse over window for a marker
    createMouseOverWindow(marker, map) {
        var name = "Unknown";
        for (var j = 0; j < users.length; j++) {
            if (users[j].i == marker.owner) {
                if (marker.team == 1) { name = '<span style="color:red">' + users[j].n + ' </span>'; }
                if (marker.team == 2) { name = '<span style="color:green">' + users[j].n + '</span>'; }
                if (marker.team == 3) { name = '<span style="color:blue">' + users[j].n + '</span>'; }
                break;
            }
        }

        marker.infowindow.setContent("<p>" + this.name + "<br />" +
            "Owner: " + name + " - " + User.GetByID(marker.owner).score + " (#" + User.GetByID(marker.owner).Rank + ")<br />" +
            "Size: " + marker.ants + "<br />" +
            "Country: " + this.country +"<br />" +
            "State: " + this.state +"<br />" +
            "Municipality: " + this.municipality +"</p>");
        marker.infowindow.open(map, marker);
    }
}