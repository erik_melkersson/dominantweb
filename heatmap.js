// This example requires the Visualization library. Include the libraries=visualization
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=visualization">

var map, redheatmap, greenheatmap, blueheatmap;
var markers = [];

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 2,
        center: { lat: 51.5074, lng: .1278 },
        mapTypeId: 'satellite'
    });

    var redlocations = []; var greenlocations = []; var bluelocations = [];

    for (i in colonies) {

        var myLatLng = { lat: colonies[i].lat, lng: colonies[i].lng };
        var markercolor; var zoomlev = map.getZoom();
        if (colonies[i].s == 1) { markercolor = '#880000'; } if (colonies[i].s == 2) { markercolor = '#008800'; }
        if (colonies[i].s == 3) { markercolor = '#000088'; }
        var marker = new google.maps.Marker({
            position: myLatLng,
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 2 * zoomlev,
                strokeWeight: 2,
                fillColor: markercolor,
                strokeColor: markercolor
            },
            visible: false,
            map: map,
            team: colonies[i].s,
            owner: colonies[i].o
        });

        markers.push(marker);

        if (colonies[i].s == 1) {
            var myLatLng = new google.maps.LatLng(colonies[i].lat, colonies[i].lng);
            redlocations.push(myLatLng);
        }
        if (colonies[i].s == 2) {
            var myLatLng = new google.maps.LatLng(colonies[i].lat, colonies[i].lng);
            greenlocations.push(myLatLng);
        }
        if (colonies[i].s == 3) {
            var myLatLng = new google.maps.LatLng(colonies[i].lat, colonies[i].lng);
            bluelocations.push(myLatLng);
        }
    }
    redheatmap = new google.maps.visualization.HeatmapLayer({
        data: redlocations,
        map: map
    });
    greenheatmap = new google.maps.visualization.HeatmapLayer({
        data: greenlocations,
        map: map
    });
    blueheatmap = new google.maps.visualization.HeatmapLayer({
        data: bluelocations,
        map: map
    });
    var redgradient = [

        'rgba(1, 1, 0, 0.5)', 'rgba(30, 1, 0, 0.5)',
        'rgba(60, 1, 0, 0.5)',
        'rgba(120, 1, 0, 0.7)', 'rgba(150, 1, 0, 0.5)',
        'rgba(180, 1, 0, 0.5)',
        'rgba(255, 1, 0, 1)'

    ]
    var greengradient = [

        'rgba(1, 1, 0, 0.5)', 'rgba(1, 30, 0, 0.5)',
        'rgba(1, 60, 0, 0.5)',
        'rgba(1, 120, 0, 0.7)', 'rgba(1, 150, 0, 0.5)',
        'rgba(1, 180, 0, 0.5)',
        'rgba(1, 255, 0, 1)'

    ]
    var bluegradient = [

        'rgba(1, 1, 1, 0.5)', 'rgba(1, 1, 30, 0.5)',
        'rgba(1, 1, 60, 0.5)',
        'rgba(1, 1, 120, 0.7)', 'rgba(1, 1, 150, 0.5)',
        'rgba(1, 1, 180, 0.5)',
        'rgba(1, 1, 255, 1)'

    ]
    var zm = map.getZoom();
    redheatmap.set('radius', 20); redheatmap.set('dissipating', true); redheatmap.set('gradient', redgradient);
    redheatmap.set('maxIntensity', 20 - zm + 4);
    redheatmap.setMap(map);
    greenheatmap.set('radius', 20); greenheatmap.set('dissipating', true); greenheatmap.set('gradient', greengradient);
    greenheatmap.set('maxIntensity', 20 - zm + 4);
    greenheatmap.setMap(map);
    blueheatmap.set('radius', 20); blueheatmap.set('dissipating', true); blueheatmap.set('gradient', bluegradient);
    blueheatmap.set('maxIntensity', 20 - zm + 4);
    blueheatmap.setMap(map);

    google.maps.event.addListener(map, 'zoom_changed', function () {
        var zoom = map.getZoom();
        var heatlvl = 11 - zoom + 4; if (heatlvl < 5) { heatlvl = 5; }
        redheatmap.set('maxIntensity', heatlvl);
        blueheatmap.set('maxIntensity', heatlvl);
        greenheatmap.set('maxIntensity', heatlvl);


        if (zoom >= 10) {
            // iterate over markers and call setVisible
            for (i = 0; i < markers.length; i++) {
                if (map.getBounds().contains(markers[i].getPosition())) {
                    markers[i].setVisible(true);
                    var markercolor;
                    if (markers[i].team == 1) { markercolor = '#880000'; } if (markers[i].team == 2) { markercolor = '#008800'; }
                    if (markers[i].team == 3) { markercolor = '#000088'; }
                    var icon =
                        {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: zoom / 2,
                            strokeWeight: 1,
                            fillOpacity: 1,
                            fillColor: markercolor,
                            strokeColor: markercolor
                        }
                    markers[i].setIcon(icon);
                } // if
            } // end of for 
        } // end of if (zomm >= 10)  
        else {
            for (i = 0; i < markers.length; i++) {
                if (map.getBounds().contains(markers[i].getPosition())) { markers[i].setVisible(false); }
            }
        }
    });  // end of zoom event listener          

} // end of init      
