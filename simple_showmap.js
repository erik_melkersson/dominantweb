var colonyArray = [];
var userArray = [];

// Returns a new Google Maps Map object
function createMap(mapElement){
    return new google.maps.Map(mapElement, {
        zoom: 2,
        center: { lat: 0, lng: 0 }
    });
}

// Return call of the Google Maps API
function initMap() {

    // Create Variables
    var mapElement = document.getElementById("map");
    var map = createMap(mapElement);
    var markers = [];

    User.LoadUsers();
    User.SetCircleUser();

    var changedFunc = function() {
        // TODO maybe the markers may be reused for better memory usage?
        for (var marker of markers) {
            marker.setMap(null);
            if(marker.circle != null){
                marker.circle.setMap(null);
            }
        }
        markers = [];
        
        var bounds = map.getBounds();
        var onMap = [];
        for (colony of colonies) {
            if (bounds.contains(colony)) { // Note: the json data has lat and lng attributes so it may be used directly
                onMap.push(colony);
            }
        }

        console.log("Found colonies on map:" + onMap.length);

        if (onMap.length > 100) {
            var clusters = clusterColonies(bounds, onMap);
            // Show the clusters
            for (var cluster of clusters) {
                // TODO Make better marker with info. Add the number of colonies on the marker somehow
                //cluster.data.length,
                var marker = new google.maps.Marker({
                    icon: { url: 'colonies.png', scaledSize: new google.maps.Size(40, 40) },
                    position: { lat: cluster.lat, lng: cluster.lng },
                    map: map
                });
                markers.push(marker);
            }
            // The remaining standalone colonies
            for (var colony of onMap) {
                markers.push(new Colony(colony).getMarker(map));
            }
        } else {
            // Just show all colonies as they are less than threshold
            for (var marker of onMap) markers.push(new Colony(marker).getMarker(map));
        }
    }

    map.addListener('dragend', changedFunc);
    map.addListener('zoom_changed', changedFunc);
    google.maps.event.addListenerOnce(map, 'idle', changedFunc);
}

// Returns a list of clusters, removes clustered colonies from onMap
function clusterColonies(bounds, onMap) {
    clusters = [];

    // Divide, just a simple grid
    
    var sw = bounds.getSouthWest();
    var ne = bounds.getNorthEast();
    var s = sw.lat();
    var n = ne.lat();
    var w = sw.lng();
    var e = ne.lng();
    
    var lngsize = e - w;
    if (e < w) lngsize += 360; // handle date border
    
    var groups = [];
    gridsize = 8;
    for (var i = 0; i < gridsize * gridsize; i++) groups[i] = [];
    
    var debugcount = 0;
    
    // Sort in groups
    for (var marker of onMap) {
        ilat = Math.floor(gridsize * (marker['lat'] - s) / (n - s));
        var eofw = marker['lng'] - w;
        if (eofw < 0) eofw += 360; // handle date border
        ilng = Math.floor(gridsize * eofw / lngsize);
        groups[ilat * gridsize + ilng].push(marker);
    }
    
    // Clear the list of stand alone nests
    onMap.splice(0, onMap.length);
    
    // Get groups with more than one and make cluster
    for (var ilat = 0; ilat < gridsize; ilat++) {
        for (var ilng = 0; ilng < gridsize; ilng++) {
            var arr = groups[ilat * gridsize + ilng];
            if (arr.length > 2) {
                // If more then 2 in an area, cluster them
                // Simple: Locate clusters in the middle of the grid
                clusters.push({'lat':s + (ilat + 0.5) * (n - s) / gridsize,
                    'lng':w + (ilng + 0.5) * lngsize / gridsize, // Note: It is ok with lng > 180, it gets wrapped
                    'data':arr}); // TODO merge arr into suitable info
            } else if (arr.length > 0) {
                 // Put them back on the list to be individually displayed
                for (var colony of arr) onMap.push(colony);
            }
        } 
    }
    return clusters;
}